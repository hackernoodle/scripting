#!/usr/bin/env bash
export LANG=C; [ ! -x "$(command -v dwarfs)" ] && echo "dwarfs not installed." && exit; [ ! -x "$(command -v bwrap)" ] && echo "bubblewrap not installed." && exit; [ ! -x "$(command -v fuse-overlayfs)" ] && echo "fuse-overlayfs not installed." && exit; cd "$(dirname "$(readlink -f "$0")")" || exit; [ "$EUID" = "0" ] && exit;

# wine
export WINE="$(command -v wine)";
export WINEPREFIX="$PWD/files/prefix"; export WINEDLLOVERRIDES="winemenubuilder.exe=d;mshtml=d;nvapi,nvapi64=n"; export WINE_LARGE_ADDRESS_AWARE=1; export RESTORE_RESOLUTION=1;

# dwarfs
bash "$PWD/settings.sh" mount; zcat "$PWD/logo.txt.gz"; echo "Support can be provided on our Matrix channel."

# auto-unmount
[ "${UNMOUNT:=1}" = "0" ] && echo "Game will not unmount automatically." || { function cleanup { cd "$OLDPWD" && bash "$PWD/settings.sh" unmount; }; trap 'cleanup' EXIT INT SIGINT SIGTERM; }

# external vulkan translation
# export DXVK_ASYNC=1
[ ! -d "$PWD/files/vulkan" ] && tar -xvf "$PWD/files/vulkan.tar.xz" -C "$PWD/files"
[ ! -f "$WINEPREFIX/vulkan.log" ] && bash "$PWD/files/vulkan/setup-vulkan.sh" && echo "Installed." > "$WINEPREFIX/vulkan.log"; export DXVK_ENABLE_NVAPI=1

# bwrap
bubblewrap_run () { [ -n "${WAYLAND_DISPLAY}" ] && export wayland_socket="${WAYLAND_DISPLAY}" || export wayland_socket="wayland-0"
[ -z "${XDG_RUNTIME_DIR}" ] && export XDG_RUNTIME_DIR="/run/user/${EUID}"
[ "${BLOCK_NET:=1}" = "0" ] && echo "Network blocking is not enabled due to user input." || UNSHARE="--unshare-net"
for s in /tmp/.X11-unix/*; do VAR+=(--bind-try "${s}" "${s}"); done
bwrap --new-session --bind / / --ro-bind-try "$HOME" "$HOME" --dev-bind /dev /dev --ro-bind-try /sys /sys --proc /proc  \
      --ro-bind-try /mnt /mnt --ro-bind-try /run /run --ro-bind-try /var /var --ro-bind-try /etc /etc \
      --ro-bind-try /tmp/.X11-unix /tmp/.X11-unix --ro-bind-try /opt /opt --bind-try /tmp /tmp \
      --ro-bind-try /usr/lib64 /usr/lib64 --ro-bind-try /usr/lib /usr/lib \
      "${VAR[@]}" "$UNSHARE" --bind-try "$PWD/files/prefix" "$(xdg-user-dir DOCUMENTS)" --bind-try "$PWD" "$PWD" "$@"; }

# start
ROOT="$PWD/files/groot"; CMD=( "$WINE" "game.exe" "$@" );
[ "${DBG:=0}" = "1" ] || { export WINEDEBUG='-all' && echo "Output muted by default. Can unmute with DBG=1." && exec &>/dev/null; }
[ "${ISOLATION:=1}" = "0" ] && echo "Isolation is disabled." && cd "$ROOT" && "${CMD[@]}" || echo "Isolation is enabled." && bubblewrap_run --chdir "$ROOT" "${CMD[@]}"
